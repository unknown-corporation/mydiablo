using Game.Scripts.Inventory.Loot;
using UnityEngine;

namespace Game.Scripts.Inventory.Potions
{
    [CreateAssetMenu(fileName = "Potion", menuName = "Inventory/Items/Potion")]

    [System.Serializable]
    public class Potion : Item 
    {
        [SerializeField] protected int healing;
    }
}