using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Inventory
{
    public class Inventory : MonoBehaviour
    {
        [SerializeField] private GameObject UiPanel;
        [SerializeField] private Transform InventoryBox;
        [SerializeField] private GameObject InventorySlot;
        public static Inventory Instance;
        public List<Item> Items = new List<Item>();
        private bool isOpened;

        private void Awake()
        {
            Instance = this;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                isOpened = !isOpened;
                if (isOpened)
                {
                    UiPanel.SetActive(true);
                }
                else
                {
                    UiPanel.SetActive(false);
                }
            }
        }

        public void AddInSlot(Item item)
        {
            Items.Add(item);
        }

        public void ListItems()
        {
            foreach (Transform item in InventoryBox)
            {
                Destroy(item.gameObject);
            }
            
            foreach (var item in Items)
            {
                GameObject obj = Instantiate(InventorySlot, InventoryBox);
                var itemIcon = obj.transform.Find("SlotIcon").GetComponent<Image>();
                itemIcon.sprite = item.icon;
            }
        }
    }
}