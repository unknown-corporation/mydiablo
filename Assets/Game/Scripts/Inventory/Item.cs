using UnityEngine;

namespace Game.Scripts.Inventory
{
    public enum ItemType {Loot, Potion, Weapon, Armor}
    public class Item : ScriptableObject
    {
        public ItemType itemType;
        public Sprite icon;
        public string itemName; 
        public int price; 
        public int weight;
    }
}