using UnityEngine;

namespace Game.Scripts.Inventory.Armor
{
    [CreateAssetMenu(fileName = "Armor", menuName = "Inventory/Items/Armor")]

    [System.Serializable]
    public class Armor : Item
    {
        [SerializeField] private int defence;
        [SerializeField] private int magicDefence;
        [SerializeField] protected int level;
    }
}