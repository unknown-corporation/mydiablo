using Game.Scripts.Inventory.Loot;
using UnityEngine;

namespace Game.Scripts.Inventory.Weapons
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Inventory/Items/Weapon")]
    
    [System.Serializable]
    public class Weapon : Item
    {
        [SerializeField] protected int damage;
        [SerializeField] protected int level;
        [SerializeField] private UseOfHands useOfHands;
        public enum UseOfHands
        {
            OneHanded,
            TwoHanded
        }
    }
}