using Game.Scripts.Player;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Image healthBarFilling;
        [SerializeField] private HealthPlayer healthPlayer;
        [SerializeField] private Gradient gradient;

        private void Awake()
        {
            healthPlayer.HealthChanged += OnHealthChanged;    // подписываемся на событие
        }

        private void OnDestroy()
        {
            healthPlayer.HealthChanged -= OnHealthChanged;    // отписываемся от события при удалинии объекта со сцены
        }

        private void OnHealthChanged(float valueAsPercantage)
        {
            healthBarFilling.fillAmount = valueAsPercantage;  // значения нашего ползунка, будут отображаться картинкой заполнения бара
            healthBarFilling.color = gradient.Evaluate(valueAsPercantage);
        }
    }
}