using UnityEngine;
using UnityEngine.AI;

namespace Game.Scripts.Animation
{
    public class animationStateController : MonoBehaviour
    {
        Animator animator;
        private int isRunningHash;
        private NavMeshAgent agent;

        private void Start()
        {
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();
            isRunningHash = Animator.StringToHash("isRunning");  // стринговое значение записываем как числовое
        }

        private void Update()
        {
            animator.SetBool(isRunningHash,agent.velocity != Vector3.zero);   // вычисляет скорость движения, если не равно нулю, то вклюяается анимация бега
        }
    }
}