using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Scripts.Enemies
{
    public class EnemieMovment : MonoBehaviour
    {
        private NavMeshAgent agent;
        private int i = -1;
        [SerializeField] private List<Transform> points;
    
        void Awake()
        {
            agent = GetComponent<NavMeshAgent>();
        }

        private void PointUpdate()
        {
            i++;
            if (i >= points.Count)  // пересчитывает число точек и потом начинает сначала
            {
                i = 0;
            }
        }

        private void Update()
        {
            if (agent.transform.position == agent.pathEndPosition)
            {
                PointUpdate();
            }

            agent.SetDestination(points[i].position);
        }
    }
}