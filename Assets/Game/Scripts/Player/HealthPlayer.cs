using System;
using UnityEngine;

namespace Game.Scripts.Player
{
    public class HealthPlayer : MonoBehaviour
    {
        [SerializeField] private PlayerData playerData;
        private int currentHealth;

        public event Action<float> HealthChanged; 

        private void Start()
        {
            currentHealth = playerData.health;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))   // проверка урона
            {
                ChangeHealth(-4);
            }
        }

        private void ChangeHealth(int value)
        {
            currentHealth += value;

            if (currentHealth <= 0)
            {
                Death();
            }
            else
            {
                float currentHealthAsPercantage = (float) currentHealth / playerData.health;
                HealthChanged?.Invoke(currentHealthAsPercantage);  // высчитываем здоровье в процентах, так как у нас HP bar в диапозоне от 0 до 1 (бегунок) 
            }
        }

        private void Death()
        {
            HealthChanged?.Invoke(0);  // HealthChanged?.Invoke() - это проверка на null
            Debug.Log("YOU ARE DEATH");
        }
    }
}