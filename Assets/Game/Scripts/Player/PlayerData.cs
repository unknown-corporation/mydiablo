using Game.Scripts.Gameplay.Characters;
using UnityEngine;

namespace Game.Scripts.Player
{
    [System.Serializable]
    public class PlayerData : СharacterData
    { 
        public int carriedWeight;
        public int maximumWeight;
        public int reputationPoints;
        public int level;
    }
}