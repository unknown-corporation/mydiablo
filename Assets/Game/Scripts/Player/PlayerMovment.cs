using UnityEngine;
using UnityEngine.AI;

namespace Game.Scripts.Player
{
    public class PlayerMovment : MonoBehaviour
    {
        private NavMeshAgent agent;
        private Camera camera1;

        void Awake()
        {
            camera1 = Camera.main;
            agent = GetComponent<NavMeshAgent>();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = camera1.ScreenPointToRay(Input.mousePosition);         // при нажатии пускаем лучь который указывает на поверхности, куда нужно двигаться 
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    agent.SetDestination(hit.point);
                }
            }
        }
    }
}
