using UnityEngine;

namespace Game.Scripts.Settings
{
    public class CameraController : MonoBehaviour
    {
        protected float CameraAngle;
        [SerializeField] protected float CameraAngleSpeed;
        [SerializeField] protected float Sensativity;
        private Camera camera1;
        private Vector3 previusMousePosition; 

        private void Start()
        {
            camera1 = Camera.main;
        }

        private void Update()
        {
            camera1.transform.position = transform.position + Quaternion.AngleAxis(CameraAngle, Vector3.up) * new Vector3(0, 8, -5);
            camera1.transform.rotation = Quaternion.LookRotation(transform.position + Vector3.up * 2f - camera1.transform.position, Vector3.up);
            
            // if (Input.GetKey(KeyCode.Q)) CameraAngle -= CameraAngleSpeed;
            // if (Input.GetKey(KeyCode.E)) CameraAngle += CameraAngleSpeed;

            if (Input.GetMouseButtonDown(1))
            {
                previusMousePosition = Input.mousePosition;
            }
            
            else if(Input.GetMouseButton(1))
            {
                var delta = Input.mousePosition - previusMousePosition;  // в переменную записываем позицыю мышки - предыдущая позиция, с предидущего кадра
                previusMousePosition = Input.mousePosition;
            
                CameraAngle += delta.x * Sensativity;
            }
        }
    }
}