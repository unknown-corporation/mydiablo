using UnityEngine;

namespace Game.Scripts.Gameplay.Characters
{
    public class СharacterData
    {
        public string name;
        public int health;  
        public int mana;
        public int growth;
        public int age;
    }
}
    