using System;
using Game.Scripts.Inventory;
using UnityEngine;

namespace Game.Scripts.Gameplay
{
    public class PickUpObjects : MonoBehaviour
    {
        public Item item;

        private void OnTriggerStay(Collider other)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Inventory.Inventory.Instance.AddInSlot(item);
                Inventory.Inventory.Instance.ListItems();        
                Destroy(gameObject);
            }
        }
    }
}