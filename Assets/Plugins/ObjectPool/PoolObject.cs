using System;
using UnityEngine;

public class PoolObject : MonoBehaviour
{
    private Action onPut = null;
    private bool isInitialized = false;

    public void Initialize(Action action)
    {
        if (isInitialized)
        {
            throw new ArgumentException("Attempt to reinitialize an initialized object");
        }

        onPut = action;
        isInitialized = true;
    }

    public void Put()
    {
        onPut?.Invoke();
        gameObject.SetActive(false);
    }
}
