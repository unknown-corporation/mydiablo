using System.Collections.Generic;
using UnityEngine;

namespace Core.Patterns
{
    public class ObjectPool<T> where T : PoolObject
    {
        private Queue<T> pool = new Queue<T>();
        private PoolObject poolObject;

        public ObjectPool(PoolObject poolObject)
        {
            this.poolObject = poolObject;
        }

        public T Get()
        {
            var obj = pool.Dequeue();
            obj.gameObject.SetActive(true);
            return obj;
        }

        public T Get(Transform transform)
        {
            T obj;

            if (pool.Count.Equals(0))
            {
                obj = Instantiate();
            }
            else
            {
                obj = pool.Dequeue();
            }

            obj.gameObject.SetActive(true);
            obj.transform.parent = transform;
            return obj;
        }

        public T Get(Vector3 position, Quaternion rotation)
        {
            T obj;

            if (pool.Count.Equals(0))
            {
                obj = Instantiate();
            }
            else
            {
                obj = pool.Dequeue();
            }

            obj.gameObject.SetActive(true);
            obj.transform.position = position;
            obj.transform.rotation = rotation;
            return obj;
        }

        public T Get(Transform transform, Vector3 position, Quaternion rotation)
        {
            T obj;

            if (pool.Count.Equals(0))
            {
                obj = Instantiate();
            }
            else
            {
                obj = pool.Dequeue();
            }

            obj.gameObject.SetActive(true);
            obj.transform.parent = transform;
            obj.transform.position = position;
            obj.transform.rotation = rotation;
            return obj;
        }


        public void Fill(int count)
        {
            for(int i =0; i < count; i++)
            {
                pool.Enqueue(Instantiate());
            }
        }

        private T Instantiate()
        {
            T poolObject = (T)Object.Instantiate(this.poolObject);
            poolObject.Initialize(() =>
            {
                Put(poolObject);
            });
            poolObject.gameObject.SetActive(false);
            return poolObject;
        }


        private void Put(T poolObject)
        {
            poolObject.transform.parent = null;
            poolObject.transform.position = Vector3.zero;
            poolObject.transform.rotation = Quaternion.Euler(0, 0, 0);

            pool.Enqueue(poolObject);
        }
    }
}

