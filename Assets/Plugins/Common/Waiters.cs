using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    public static class Waiters
    {
        private static readonly Dictionary<float, WaitForSeconds> Values = new Dictionary<float, WaitForSeconds>();
        private static readonly Dictionary<float, WaitForSecondsRealtime> Unscaled = new Dictionary<float, WaitForSecondsRealtime>();

        public static readonly WaitForFixedUpdate FixedUpdate = new WaitForFixedUpdate();
        public static readonly WaitForEndOfFrame EndFrame = new WaitForEndOfFrame();


        public static WaitForSeconds Wait(float value, bool cache = false)
        {
            if (value <= 0)
                return null;

            if (Values.TryGetValue(value, out WaitForSeconds wait))
                return wait;

            wait = new WaitForSeconds(value);
            if (cache)
                Values.Add(value, wait);
            return wait;
        }

        public static WaitForSecondsRealtime WaitUnscaled(float value, bool cache = false)
        {
            if (value <= 0)
                return null;

            if (Unscaled.TryGetValue(value, out WaitForSecondsRealtime wait))
                return wait;

            wait = new WaitForSecondsRealtime(value);
            if (cache)
                Unscaled.Add(value, wait);
            return wait;
        }
    }
}