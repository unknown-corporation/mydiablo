﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FloatingJoystick : JoystickBase
{
    public bool interactable = true;
    [SerializeField] private bool visible = true;

    protected override void Awake()
    {
        base.Awake();
        background.gameObject.SetActive(false);
    }

    protected override void BeginDrag(PointerEventData eventData)
    {
        if (!interactable)
            return;

        background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
        background.gameObject.SetActive(visible);
        base.BeginDrag(eventData);
    }

    protected override void Drag(PointerEventData eventData)
    {
        if (!interactable)
            return;
        base.Drag(eventData);
    }

    protected override void TryEndDrag()
    {
        background.gameObject.SetActive(false);
        base.TryEndDrag();
    }
}