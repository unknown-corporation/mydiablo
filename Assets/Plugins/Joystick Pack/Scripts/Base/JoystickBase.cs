﻿using UnityEngine;
using UnityEngine.EventSystems;

public abstract class JoystickBase : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public delegate void DragEvent(in Vector2 input);

        public event System.Action OnInputStart;
        public event DragEvent OnInput;
        public event System.Action OnInputEnd;


        [SerializeField, Min(0)] private float threshold = 0.1f;
        [SerializeField] protected RectTransform background;
        [SerializeField] private RectTransform handle;
        public bool clamped;


        private RectTransform _baseRect;
        private float _thresholdSqr;
        private Vector2 _radius;
        private Canvas _canvas;
        private Camera _cam;
        private Vector2 _direction;
        private bool _dragging;

        public bool Dragging => _dragging;
        public Vector2 Direction => _direction;
        public float Horizontal => _direction.x;
        public float Vertical => _direction.y;

        public float Threshold
        {
            get => threshold;
            set
            {
                threshold = value;
                _thresholdSqr = Mathf.Pow(value, 2);
            }
        }

        protected virtual void Awake()
        {
            Threshold = threshold;

            _canvas = GetComponentInParent<Canvas>();
            _baseRect = transform as RectTransform;
            if (_canvas == null)
                Debug.LogError("The Joystick is not placed inside a canvas");

            Vector2 center = new Vector2(0.5f, 0.5f);
            background.pivot = center;
            handle.anchorMin = center;
            handle.anchorMax = center;
            handle.pivot = center;
            handle.anchoredPosition = Vector2.zero;

            _radius = new Vector2((background.sizeDelta.x - handle.sizeDelta.x) / 2, 0);
            _radius.y = _radius.x;
        }

        protected virtual void OnDisable()
        {
            TryEndDrag();
        }


        protected virtual void BeginDrag(PointerEventData eventData)
        {
            if (_dragging)
                return;

            if (_canvas.renderMode == RenderMode.ScreenSpaceCamera)
                _cam = _canvas.worldCamera;

            _dragging = true;
            OnInputStart?.Invoke();
            HandleInput(eventData);
        }

        protected virtual void Drag(PointerEventData eventData)
        {
            HandleInput(eventData);
        }

        protected virtual void TryEndDrag()
        {
            if (!_dragging)
                return;

            _dragging = false;
            _direction = Vector2.zero;
            handle.anchoredPosition = Vector2.zero;
            OnInputEnd?.Invoke();
        }


        private void HandleInput(PointerEventData eventData)
        {
            Vector2 position = RectTransformUtility.WorldToScreenPoint(_cam, background.position);
            _direction = (eventData.position - position) / (_radius * _canvas.scaleFactor);
            float sqrMagnitude = _direction.sqrMagnitude;

            if (sqrMagnitude < _thresholdSqr)
                _direction = Vector2.zero;
            else if (sqrMagnitude > 1 && clamped)
                _direction.Normalize();

            handle.anchoredPosition = _direction * _radius;

            if (sqrMagnitude >= _thresholdSqr)
                OnInput?.Invoke(_direction);
        }


        protected Vector2 ScreenPointToAnchoredPosition(in Vector2 screenPosition)
        {
            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(_baseRect, screenPosition, _cam, out Vector2 localPoint))
                return Vector2.zero;

            Vector2 pivotOffset = _baseRect.pivot * _baseRect.sizeDelta;
            return localPoint - (background.anchorMax * _baseRect.sizeDelta) + pivotOffset;
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            Drag(eventData);
        }

        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
        {
            BeginDrag(eventData);
        }

        void IEndDragHandler.OnEndDrag(PointerEventData eventData)
        {
            TryEndDrag();
        }
    }