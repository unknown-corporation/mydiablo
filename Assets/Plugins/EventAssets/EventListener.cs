using System.Collections;
using Common;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Gameplay.Events
{
    [DefaultExecutionOrder(-1000)]
    public abstract class EventListenerBase : MonoBehaviour, IPrioritized
    {
        protected class EventAdvancedAttribute : FoldoutGroupAttribute
        {
            protected const string BoxName = "Advanced";
            public EventAdvancedAttribute(float order = 0) : base(BoxName, order) { }
            public EventAdvancedAttribute(bool expanded, float order = 0) : base(BoxName, expanded, order) { }
        }

        [EventAdvanced] [SerializeField] private Priority priority = Priority.Medium;
        [EventAdvanced] [SerializeField] private bool useAwakeAndDestroy;
        [EventAdvanced] [SerializeField] protected bool delayed;

        [EventAdvanced]
        [ShowIf(nameof(delayed))]
        [Tooltip("In seconds, waits at least a frame")]
        [SerializeField, Min(0)]
        protected float delay;

        [EventAdvanced]
        [ShowIf(nameof(delayed))]
        [Tooltip("Allow multiple delayed calls")]
        [SerializeField]
        protected bool allowMultiple;

        protected int DelayedRoutines;


        public Priority Priority => priority;


        protected virtual void Awake()
        {
            if (useAwakeAndDestroy)
                Subscribe();
        }

        protected virtual void OnDestroy()
        {
            if (useAwakeAndDestroy)
            {
                StopAllCoroutines();
                Unsubscribe();
            }
        }

        protected virtual void OnEnable()
        {
            if (!useAwakeAndDestroy)
                Subscribe();
        }

        protected virtual void OnDisable()
        {
            if (!useAwakeAndDestroy)
            {
                StopAllCoroutines();
                Unsubscribe();
            }
        }

        protected abstract void Subscribe();
        protected abstract void Unsubscribe();
    }


    public sealed class EventListener : EventListenerBase, IListener
    {
        [SerializeField] private bool raiseOnStart;

        [PropertyOrder(-1)] [SerializeField] private AssetList<IListener>[] events;
        [Space] [SerializeField] private UnityEvent onRaise;

        private void Start()
        {
            if (raiseOnStart)
                ((IListener)this).Execute();
        }


        void IListener.Execute()
        {
            if (delayed)
            {
                if (allowMultiple || DelayedRoutines == 0)
                    StartCoroutine(DelayedRaise());
            }
            else
            {
                OnRaise();
            }
        }


        private IEnumerator DelayedRaise()
        {
            DelayedRoutines++;
            yield return Waiters.Wait(delay, true);
            OnRaise();
            DelayedRoutines--;
        }

        private void OnRaise()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            EventsLogger.Log(this);
#endif
            onRaise.Invoke();
        }

        protected override void Subscribe()
        {
            foreach (var eventAsset in events)
                eventAsset.Add(this);
        }

        protected override void Unsubscribe()
        {
            foreach (var eventAsset in events)
                eventAsset.Remove(this);
        }
    }

    public class EventListener<T> : EventListenerBase, IListener<T>
    {
        [PropertyOrder(-1)] [SerializeField] private EventAsset<T>[] events;

        [PropertyOrder(int.MaxValue)]
        [Space]
        [SerializeField]
        private UnityEvent<T> onRaise;

        void IListener<T>.Execute(in T a)
        {
            if (delayed)
            {
                if (allowMultiple || DelayedRoutines == 0)
                    StartCoroutine(DelayedRaise(a));
            }
            else
            {
                OnRaise(a);
            }
        }

        private IEnumerator DelayedRaise(T a)
        {
            DelayedRoutines++;
            yield return Waiters.Wait(delay, true);
            OnRaise(a);
            DelayedRoutines--;
        }

        protected virtual void OnRaise(in T a)
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            EventsLogger.Log(this);
#endif
            onRaise.Invoke(a);
        }

        protected override void Subscribe()
        {
            foreach (var eventAsset in events)
                eventAsset.Add(this);
        }

        protected override void Unsubscribe()
        {
            foreach (var eventAsset in events)
                eventAsset.Remove(this);
        }
    }

    public class EventListener<T, T1> : EventListenerBase, IListener<T, T1>
    {
        [PropertyOrder(-1)] [SerializeField] private EventAsset<T, T1>[] events;

        [PropertyOrder(int.MaxValue)]
        [SerializeField]
        private UnityEvent<T, T1> onRaise;

        void IListener<T, T1>.Execute(in T a, in T1 b)
        {
            if (delayed)
            {
                if (allowMultiple || DelayedRoutines == 0)
                    StartCoroutine(DelayedRaise(a, b));
            }
            else
            {
                OnRaise(a, b);
            }
        }

        private IEnumerator DelayedRaise(T a, T1 b)
        {
            DelayedRoutines++;
            yield return Waiters.Wait(delay, true);
            OnRaise(a, b);
            DelayedRoutines--;
        }

        protected virtual void OnRaise(in T a, T1 b)
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            EventsLogger.Log(this);
#endif
            onRaise.Invoke(a, b);
        }

        protected override void Subscribe()
        {
            foreach (var eventAsset in events)
                eventAsset.Add(this);
        }

        protected override void Unsubscribe()
        {
            foreach (var eventAsset in events)
                eventAsset.Remove(this);
        }
    }
}