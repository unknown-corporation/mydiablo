using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Gameplay.Events
{
    [CreateAssetMenu(menuName = "Events/New Event Asset")]
    public class EventAsset : AssetList<IListener>, IEvent
    {
        [SerializeField] private UnityEvent onInvoke;

        [HideInEditorMode, Button]
        public void Invoke()
        {
            onInvoke.Invoke();
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            EventsLogger.Log(this, true, Count == 0);
#endif
            foreach (Priority priority in Priorities)
            {
                var list = Listeners[priority];
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        list[i].Execute();
                    }
                    catch (ArgumentOutOfRangeException) { }
                }
            }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            EventsLogger.Log(this, false, Count == 0);
#endif
        }
    }

    public class EventAsset<T> : AssetList<IListener>, IEvent<T>
    {
        [SerializeField] private UnityEvent<T> onInvoke;

        [ShowInInspector, ReadOnly]
        protected readonly Dictionary<Priority, List<IListener<T>>> ListenersGeneric = new Dictionary<Priority, List<IListener<T>>>
        {
            { Priority.High, new List<IListener<T>>() },
            { Priority.Medium, new List<IListener<T>>() },
            { Priority.Low, new List<IListener<T>>() }
        };

        [HideInEditorMode, Button]
        public void Invoke(T a)
        {
            onInvoke.Invoke(a);
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            EventsLogger.Log(this, a, true, GenericCount + Count == 0);
#endif

            foreach (Priority priority in Priorities)
            {
                var genericsList = ListenersGeneric[priority];
                for (int i = genericsList.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        genericsList[i].Execute(a);
                    }
                    catch (ArgumentOutOfRangeException) { }
                }

                var list = Listeners[priority];
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        list[i].Execute();
                    }
                    catch (ArgumentOutOfRangeException) { }
                }
            }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
            EventsLogger.Log(this, a, false, GenericCount + Count == 0);
#endif
        }

        public bool Add(in IListener<T> value)
        {
            var list = ListenersGeneric[value.Priority];
            int index = list.IndexOf(value);
            if (index >= 0)
                return false;

            list.Add(value);
            return true;
        }

        public bool Remove(in IListener<T> value)
        {
            return ListenersGeneric[value.Priority].Remove(value);
        }


        private int GenericCount
        {
            get
            {
                int sum = 0;
                foreach (var listener in ListenersGeneric)
                    sum += listener.Value.Count;

                return sum;
            }
        }
    }

    public class EventAsset<T, T1> : AssetList<IListener>, IEvent<T, T1>
    {
        [SerializeField] private UnityEvent<T, T1> onInvoke;

        [ShowInInspector, ReadOnly]
        protected readonly Dictionary<Priority, List<IListener<T, T1>>> ListenersGeneric = new Dictionary<Priority, List<IListener<T, T1>>>
        {
            { Priority.High, new List<IListener<T, T1>>() },
            { Priority.Medium, new List<IListener<T, T1>>() },
            { Priority.Low, new List<IListener<T, T1>>() }
        };

        [HideInEditorMode, Button]
        public void Invoke(T a, T1 b)
        {
            onInvoke.Invoke(a, b);
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            EventsLogger.Log(this, a, b, true, GenericCount + Count == 0);
#endif
            foreach (Priority priority in Priorities)
            {
                var genericsList = ListenersGeneric[priority];
                for (int i = genericsList.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        genericsList[i].Execute(a, b);
                    }
                    catch (ArgumentOutOfRangeException) { }
                }

                var list = Listeners[priority];
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        list[i].Execute();
                    }
                    catch (ArgumentOutOfRangeException) { }
                }
            }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            EventsLogger.Log(this, a, b, false, GenericCount + Count == 0);
#endif
        }

        public bool Add(in IListener<T, T1> value)
        {
            var list = ListenersGeneric[value.Priority];
            int index = list.IndexOf(value);
            if (index >= 0)
                return false;

            list.Add(value);
            return true;
        }

        public bool Remove(in IListener<T, T1> value)
        {
            return ListenersGeneric[value.Priority].Remove(value);
        }

        private int GenericCount
        {
            get
            {
                int sum = 0;
                foreach (var listener in ListenersGeneric)
                    sum += listener.Value.Count;

                return sum;
            }
        }
    }
}