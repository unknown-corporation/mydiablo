﻿namespace Gameplay.Events
{
    public enum Priority : byte
    {
        Low = 0,
        Medium = 32,
        High = 64
    }
    public interface IPrioritized
    {
        Priority Priority { get; }
    }
}