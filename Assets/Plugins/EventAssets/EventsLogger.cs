﻿using UnityEngine;

namespace Gameplay.Events
{
#if UNITY_EDITOR || DEVELOPMENT_BUILD
    public static class EventsLogger
    {
        private static string prefix = string.Empty;
        private static readonly string Space = "   ";

        private const string EventColor = "cyan";
        private const string ListenerColor = "green";
        private const string EmptyColor = "yellow";

        public static void Log(EventListener listener)
        {
            Debug.Log(Listener(listener), listener);
        }

        public static void Log<T>(EventListener<T> listener)
        {
            Debug.Log($"{Listener(listener)}({typeof(T).Name})", listener);
        }

        public static void Log<T, T1>(EventListener<T, T1> listener)
        {
            Debug.Log($"{Listener(listener)}({typeof(T).Name}, {typeof(T1).Name})", listener);
        }

        public static void Log(EventAsset eventAsset, bool start, bool empty)
        {
            if (start)
                Debug.Log($"{Event(eventAsset)}(){Empty(empty)}", eventAsset);
            prefix = start
                ? $"{prefix}{Space}"
                : prefix.Remove(prefix.Length - Space.Length);
        }

        public static void Log<T>(EventAsset<T> eventAsset, in T a, bool start, bool empty)
        {
            if (start)
                Debug.Log($"{Event(eventAsset)}({typeof(T).Name} {(a is Object obj ? obj.name : a.ToString())}){Empty(empty)}", eventAsset);
            prefix = start
                ? $"{prefix}{Space}"
                : prefix.Remove(prefix.Length - Space.Length);
        }

        public static void Log<T, T1>(EventAsset<T, T1> eventAsset, in T a, in T1 b, bool start, bool empty)
        {
            if (start)
            {
                Debug.Log(
                    $"{Event(eventAsset)}({typeof(T).Name} {(a is Object obj ? obj.name : a.ToString())}, {typeof(T1).Name} {(b is Object objB ? objB.name : b.ToString())}){Empty(empty)}",
                    eventAsset);
            }

            prefix = start
                ? $"{prefix}{Space}"
                : prefix.Remove(prefix.Length - Space.Length);
        }

        private static string Event(Object assetEvent)
        {
            return $"{prefix}Event <color={EventColor}>{assetEvent.name}</color> Invoked";
        }

        private static string Listener(Object listener)
        {
            return $"{prefix}Listener <color={ListenerColor}>{listener.name}</color> Raised";
        }

        private static string Empty(bool value)
        {
            return value ? $" <color={EmptyColor}>Empty</color>" : string.Empty;
        }
    }
#endif
}