﻿namespace Gameplay.Events
{
    public interface IEvent<T, T1> : IListenerContainer<IListener<T, T1>>, IListenerContainer<IListener>
    {
        void Invoke(T a, T1 b);
    }

    public interface IEvent<T> : IListenerContainer<IListener<T>>, IListenerContainer<IListener>
    {
        void Invoke(T a);
    }

    public interface IEvent : IListenerContainer<IListener>
    {
        void Invoke();
    }
}