﻿namespace Gameplay.Events
{
    public interface IListenerContainer<T>
    {
        bool Add(in T value);
        bool Remove(in T value);
        
        int Count { get; }
    }
}