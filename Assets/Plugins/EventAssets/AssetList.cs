﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gameplay.Events
{
    public abstract class AssetList<T> : ScriptableObject, IListenerContainer<T> where T : IPrioritized
    {
        // ReSharper disable once StaticMemberInGenericType
        protected static readonly Priority[] Priorities = { Priority.High, Priority.Medium, Priority.Low };


        [ShowInInspector, ReadOnly]
        protected readonly Dictionary<Priority, List<T>> Listeners = new Dictionary<Priority, List<T>>
        {
            { Priority.High, new List<T>() },
            { Priority.Medium, new List<T>() },
            { Priority.Low, new List<T>() }
        };

        public bool Add(in T value)
        {
            var list = Listeners[value.Priority];

            int index = list.IndexOf(value);
            if (index >= 0)
                return false;

            list.Add(value);
            return true;
        }

        public bool Remove(in T value)
        {
            return Listeners[value.Priority].Remove(value);
        }

        public int Count
        {
            get
            {
                int sum = 0;
                foreach (var listener in Listeners) 
                    sum += listener.Value.Count;

                return sum;
            }
        }
    }
}